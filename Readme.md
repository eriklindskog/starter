# Exempelprojekt Hackathon

Det här projektet kan användas som bas för att komma igång med spring-boot.
Det finns två controllers, en för REST och en för HTML(som använder Thymeleaf som templateenginge).

Om du inte redan är bekant med Spring så kan det vara bra att läsa på lite på https://spring.io/docs.
Det finns lite enkla tutorials och annat nyttigt där.
Det här projektet till exempel är en kombination av två tutorials för att få med både REST och HTML i samma projekt.

Det kan vara lite knep och knåp med att få upp miljön, men det ska ni ha fått information om på annat håll.